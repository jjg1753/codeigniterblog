# CodeIgniterBlog

 A PHP Codeigniter blog application with CRUD functionality.
 
###################
CodeIgnitorBlog
###################

This is a simple blog application built on CodeIgniter 3.x. Inspired by the YouTube series [Build a CodeIgniter PHP App](https://www.youtube.com/watch?v=I752ofYu7ag)

*******************
Usage
*******************

Create the database with the posts table and upload to your host.