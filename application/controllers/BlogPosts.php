<?php
  class BlogPosts extends CI_Controller{
    public function index() {
      $data['title'] = 'Recent Posts';

      $data['posts'] = $this->Post_model->get_posts();

      $this->load->view('templates/header');
      $this->load->view('blogposts/index', $data);
      $this->load->view('templates/footer');

    }

    public function view($slug = NULL){
      $data['post'] = $this->Post_model->get_posts($slug);
      $post_id = $data['post']['id'];
      $data['comments'] = $this->comment_model->get_comments($post_id);
      if(empty($data['post'])){
        show_404();
      }

      $data['title'] = $data['post']['title'];

      $this->load->view('templates/header');
      $this->load->view('blogposts/view', $data);
      $this->load->view('templates/footer');
    }

    public function create(){
      $data['title'] = 'Create a new blog post';

      //Fetching the categories to populate the dropdown with categories
      $data['categories'] = $this->Post_model->get_categories();

      $this->form_validation->set_rules('title', 'Title', 'required');
      $this->form_validation->set_rules('body', 'Body', 'required');

      if($this->form_validation->run() === FALSE){

      $this->load->view('templates/header');
      $this->load->view('blogposts/create', $data);
      $this->load->view('templates/footer');
    } else {
      //To upload image
      $config['upload_path'] = './assets/images/posts';
      $config['allowed_types'] = 'gif|jpeg|png';
      $config['max_size'] = '2048';
      $config['max_width'] = '1024';
      $config['max_height'] = '768';

      $this->load->library('upload', $config);

      if(!$this->upload->do_upload()) {
        $errors = array('error' => $this->upload->display_errors());
        $post_image = 'noimage.jpeg';
      } else {
        $data = array('upload_data' => $this->upload->data());
        $post_image = $_FILES['userfile']['name'];
      }

      $this->Post_model->create_post($post_image);
      redirect('blogposts');
    }
  }

    public function delete($id){
      $this->Post_model->delete_post($id);
      redirect('blogposts');
    }

    public function edit($slug) {
      $data['post'] = $this->Post_model->get_posts($slug);

      if(empty($data['post'])){
        show_404();
      }

      //Fetching the categories to populate the dropdown with categories
      $data['categories'] = $this->Post_model->get_categories();
      $data['title'] = 'Edit Post';

      $this->load->view('templates/header');
      $this->load->view('blogposts/edit', $data);
      $this->load->view('templates/footer');
    }

    public function update() {
      $this->Post_model->update_post();
      redirect('blogposts');
    }
}
 ?>
