<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['blogposts/update'] = 'blogposts/update';
$route['blogposts/create'] = 'blogposts/create';
$route['blogposts/(:any)'] = 'blogposts/view/$1';
$route['blogposts'] = 'blogposts/index';

$route['default_controller'] = 'pages/view';

$route['categories'] = 'categories/index';
$route['categories/create_category'] = 'categories/create';
$route['categories/posts/(:any)'] = 'categories/posts/$1';

$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
