<h2><?php echo $post['title']; ?></h2>
<small class="post-date">Posted On: <?php echo $post['created_at']; ?></small><br />
  <img src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image'] ?>">
<div class="post-body">
  <?php echo $post['body']; ?>
</div>

<hr>
<a class="btn btn-info float-left mr-3" role="button" href="<?php echo base_url(); ?>blogposts/edit/<?php echo $post['slug'];?>">Edit Post</a>

<?php echo form_open('/blogposts/delete/'.$post['id']); ?>
  <input type="submit" value="Delete Post" class="btn btn-danger" />
</form>
<hr />

<h3>Comments</h3>
<?php if($comments) : ?>
  <?php foreach($comments as $comment) : ?>
    <div class="card card-body bg-light">
      <h5><?php echo $comment['body']; ?> [by <strong><?php echo $comment['name']; ?></strong>]</h5>

    </div>
    <br />
  <?php endforeach; ?>
<?php else : ?>
  <p>
    No Comments yet on this post to be displayed :(
  </p>
  <p>
    Be the first to comment on this post!
  </p>
<?php endif; ?>

<hr />
<h3>Add Comments</h3>
<?php echo validation_errors(); ?>
<?php echo form_open('comments/create/'.$post['id']); ?>
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" class="form-control" />
  </div>
  <div class="form-group">
    <label>Email</label>
    <input type="text" name="email" class="form-control" />
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea id="editor1" name="body" class="form-control"></textarea>
  </div>
  <input type="hidden" name="slug" value="<?php echo $post['slug']; ?>">
  <button class="btn btn-primary" type="submit">Post Comment</button>
</form>
