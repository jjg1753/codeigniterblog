<h2><?= $title?></h2>
<?php foreach($posts as $post): ?>
  <h3><?php echo $post['title'];?></h3>
  <div class="row">
    <div class="col-sm-4">
      <img style="width:100%;" src="<?php echo site_url(); ?>assets/images/posts/<?php echo $post['post_image'] ?>">
    </div>
    <div class="col-sm-8">
      <small class="post-date">Posted On: <?php echo $post['created_at']; ?> in <strong><?php echo $post['name']; ?></strong></small><br />
      <?php echo word_limiter($post['body'], 50); ?>
      <br /><br />
      <p>
        <a class="btn btn-info" role="button" href="<?php echo site_url('/blogposts/'.$post['slug']); ?>"> Read More </a>
      </p>
    </div>
  </div>
  <br />
<?php endforeach; ?>
