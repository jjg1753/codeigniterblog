<h2><?= $title; ?></h2>

<?php echo validation_errors(); ?>

<?php echo form_open('blogposts/update'); ?>
<input type="hidden" name="id" value="<?php echo $post['id']; ?>">
  <div class="form-group">
    <label >Title</label>
    <input type="type" class="form-control" name="title" placeholder="Enter a title" value="<?php echo $post['title']; ?>">
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea id="editor1" class="form-control" name="body" placeholder="Enter the body for your blog post" ><?php echo $post['body']; ?></textarea>
  </div>
  <div class="form-group">
    <label>Category</label>
    <select name="category_id" class="form-control">
      <?php foreach($categories as $category): ?>
        <option <?php if($post['name'] === $category['name']) : ?> selected="selected" <?php endif; ?>  value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
      <?php endforeach; ?>
    </select>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
